package com.tsc.afedorovkaritsky.tm.api.service;

import com.tsc.afedorovkaritsky.tm.enumerated.Status;
import com.tsc.afedorovkaritsky.tm.model.Project;

import java.util.List;

public interface IProjectService {

    void create(String name);

    void create(String name, String description);

    void add(Project project);

    void remove(Project project);

    Project findProjectById(String id);

    Project findProjectByName(String name);

    Project findProjectByIndex(Integer index);

    Project removeProjectByIndex(Integer index);

    Project removeProjectByName(String name);

    Project removeProjectById(String id);

    Project updateProjectById(String id, String name, String description);

    Project updateProjectByIndex(Integer index, String name, String description);

    Project startProjectById(String id);

    Project startProjectByName(String name);

    Project startProjectByIndex(Integer index);

    Project finishProjectById(String id);

    Project finishProjectByName(String name);

    Project finishProjectByIndex(Integer index);

    Project changeStatusProjectById(String id, Status status);

    Project changeStatusProjectByName(String name, Status status);

    Project changeStatusProjectByIndex(Integer index, Status status);

    List<Project> findAll();

    void clear();

}
