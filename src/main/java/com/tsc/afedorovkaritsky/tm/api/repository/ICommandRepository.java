package com.tsc.afedorovkaritsky.tm.api.repository;

import com.tsc.afedorovkaritsky.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
